# Toko Produk FSW 2
## Widias/19081000011/2D

### Deskripsi Project
Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa sed iste temporibus voluptatum sint ducimus aperiam eaque, quae maxime dolor autem fuga, ipsam facilis unde commodi excepturi. Nemo, optio impedit?
Numquam rem, deleniti autem animi qui commodi quo quisquam ullam sit iusto ipsa blanditiis porro dolor maxime iste voluptas debitis? Nesciunt, nobis sunt fuga officiis odit sed necessitatibus dolorum aliquam!

### Instalasi
1. buka terminal
2. ketik "npm install" pada terminal
3. sesuaikan username dan password pada config/config.json lalu save
4. ketik "npx sequelize db:create" pada terminal untuk generate database pada local
5. ketik "npx sequelize db:migrate" pada terminal untuk migrasi tabel
6. ketik "npm run dev" pada terminal untuk menjalankan app
7. buka link "http://localhost:3000/admin/product" pada browser

### ERD
![Alt text](/ERD_widias.png "a title")

